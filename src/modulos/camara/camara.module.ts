import { Module } from '@nestjs/common';
import { CamaraService } from './camara.service';
import { CamaraController } from './camara.controller';
import {TypeOrmModule} from "@nestjs/typeorm";
import {CamaraEntity} from "./camara.entity";

@Module({
  imports: [
      TypeOrmModule.forFeature([CamaraEntity], 'default')
  ],
  providers: [CamaraService],
  controllers: [CamaraController]
})
export class CamaraModule {}
