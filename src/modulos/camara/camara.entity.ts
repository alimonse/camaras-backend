import {Column, Entity, PrimaryGeneratedColumn} from "typeorm";

@Entity('CAMARA')
export class CamaraEntity {
    @PrimaryGeneratedColumn({
        name: 'ID',
        unsigned: true,
    })
    id: number

    @Column({
        name: "nombre_camara",
        type: "varchar",
        nullable: false,
    })
    nombreCamara: string

    @Column({
        name: "serial",
        type: "varchar",
        nullable: false,
    })
    serial: string

    @Column({
        name: "tipo",
        type: "varchar",
        nullable: true,
    })
    tipo: string


}
