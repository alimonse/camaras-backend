import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import {TypeOrmModule} from "@nestjs/typeorm";
import {CamaraEntity} from "./modulos/camara/camara.entity";
import {CamaraModule} from "./modulos/camara/camara.module";

@Module({
  imports: [
      CamaraModule,
    TypeOrmModule.forRoot({
      type: "postgres",
      host: 'localhost',
      port: 49153,
      username: 'admin',
      password: 'admin',
      database: 'postgres',
      entities: [CamaraEntity],
      synchronize: true,
      schema: 'public'
      // dropSchema:true
    }),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
